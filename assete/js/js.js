
	var intlog = 0;
	var prefixlog = 0;
	var fnamelog = 0;
	var snamelog = 0;
	var addresslog = 0;
    var emaillog = 0;
    var tellog = 0;
	var provincelog = 0;
	var amphuelog = 0;
	var zipcodelog = 0;
	var districtlog = 0;
	var idcartlog = 0;
	var technidlog = 0;
	var conditionlog = 0;
	
	$("#collapseTechnid").collapse("show");

	$("#prefix-box").hide();
	$("#prefix-select").change(function(){ 
		if($(this).val() != null && $(this).val() != ""){
			switch($(this).val()){
				case '1':
					$("#prefix-box").hide();
					$("#prefix").val('นาย');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '2':
					$("#prefix-box").hide();
					$("#prefix").val('นาง');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '3':
					$("#prefix-box").hide();
					$("#prefix").val('นางสาว');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '4':
					$("#prefix").val('');
					$("#fname-box").removeClass("col-lg-5");
					$("#sname-box").removeClass("col-lg-5");
					$("#prefix-box").show();
					$("#fname-box").addClass("col-lg-4");
					$("#sname-box").addClass("col-lg-4");
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
					
				break;
			}
		}else{
			$('#prefix-selectHelp').text("กรุณาระบุข้อมูล");
			$("#prefix-select").addClass("is-invalid");
		}
	});

	$("#prefix").change(function(){ 
		if($("#prefix").val() != null && $("#prefix").val() != ""){
			$('#prefixHelp').text("");
			$("#prefix").removeClass("is-invalid");
			if(prefixlog > 0){--prefixlog;}
		}else{
			$("#prefix").addClass("is-invalid");
			$('#prefixHelp').text("กรุณาระบุข้อมูล");
			if(prefixlog == 0){prefixlog++;}
		}
	});
	 
	$("#fname").change(function(){ 
		if($("#fname").val() != null && $("#fname").val() != ""){
			$('#fnameHelp').text("");
			$("#fname").removeClass("is-invalid");
			if(fnamelog > 0){--fnamelog;}
		}else{
			$('#fnameHelp').text("กรุณาระบุข้อมูล");
			if(fnamelog == 0){fnamelog++;}
			$("#fname").addClass("is-invalid");
		}
	});
	
	$("#sname").change(function(){ 
		if($("#sname").val() != null && $("#sname").val() != ""){
			$('#snameHelp').text("");
			$("#sname").removeClass("is-invalid");
			if(snamelog > 0){--snamelog;}
		}else{
			$('#snameHelp').text("กรุณาระบุข้อมูล");
			$("#sname").addClass("is-invalid");
			if(snamelog == 0){snamelog++;}
		}
	});

	$("#email").change(function(){ 
		if($("#email").val() != null && $("#email").val() != ""){
			if(Email_format($("#email").val())){
				$('#emailHelp').text("");

				$("#email").removeClass("is-invalid");
				if(emaillog > 0){--emaillog;}
			}else{
				$("#email").addClass("is-invalid");
				if(emaillog == 0){emaillog++;}
				$('#emailHelp').text("กรุณาระบุข้อมูลอีเมล์ให้ถูกต้อง");
			}
		}else{
			$('#emailHelp').text("");
			$("#email").removeClass("is-invalid");
			if(emaillog > 0){--emaillog;}
		}
    });
    
    var telid = "";
	$('#tel').keyup(function(e) {
		
		if(e.key != "Backspace"){
			if ($.isNumeric(e.key)) {
				telid += e.key;
				switch(telid.length){
					case 3:
						telid += "-";
						break;
					case 7:
						telid += "-";
						break;
				}
				this.value = telid;
			}
		}else{
			if(this.value.length == 0){
				this.value = "";
			}
			telid = this.value;
		}
	});
    $("#tel").change(function(){ 
		if(this.value != null && this.value != ""){
            console.log(this.value); 
            if(this.value.length == 12){
                if(Tel_format(this.value)){
                    $('#telHelp').text("");

                    $("#tel").removeClass("is-invalid");
                    if(tellog > 0){--tellog;}
                }else{
                    $("#tel").addClass("is-invalid");
                    if(tellog == 0){tellog++;}
                    $('#telHelp').text("กรุณาระบุข้อมูลเบอร์โทรศัพท์มือถือให้ถูกต้อง");
                }
            }
            else{
                console.log(this.value.length);
                $("#tel").addClass("is-invalid");
                if(tellog == 0){tellog++;}
                $('#telHelp').text("กรุณาระบุข้อมูลเบอร์โทรศัพท์มือถือให้ถูกต้อง");
            }
		}else{
            $('#telHelp').text("");

            $("#tel").removeClass("is-invalid");
            if(tellog > 0){--tellog;}

        }
    });

	$("#address").change(function(){ 
		if($("#address").val() != null && $("#address").val() != ""){
			$('#addressHelp').text("");
			$("#address").removeClass("is-invalid");
			if(addresslog > 0){--addresslog;}
		}else{
			$('#addressHelp').text("กรุณาระบุข้อมูล");
			$("#address").addClass("is-invalid");
			if(addresslog == 0){addresslog++;}
		}
	});

	$("#zipcode").change(function(){ 
		if($("#zipcode").val() != null && $("#zipcode").val() != ""){
			$('#zipcodeHelp').text("");
			$("#zipcode").removeClass("is-invalid");
			if(zipcodelog > 0){--zipcodelog;}
		}else{
			$('#zipcodeHelp').text("กรุณาระบุข้อมูล");
			if(zipcodelog == 0){zipcodelog++;}
			$("#zipcode").addClass("is-invalid");
		}
	});

    var dartid = "";
	$('#idcart').keyup(function(e) {
		
		if(e.key != "Backspace"){
			if ($.isNumeric(e.key)) {
				dartid += e.key;
				switch(dartid.length){
					case 1:
						dartid += "-";
						break;
					case 6:
						dartid += "-";
						break;
					case 12:
						dartid += "-";
						break;
					case 15:
						dartid += "-";
						break;
				}
				
				this.value = dartid;
			}
		}else{
			if(this.value.length == 0){
				this.value = "";
			}
			dartid = this.value;
		}
	});
	$("#idcart").change(function(){ 
        var idcard = $("#idcart").val();
        //console.log(idcard);
		if( idcard != null && idcard != "" ){
            console.log(idcard);
			$('#idcartHelp').text("");
			$("#idcart").removeClass("is-invalid");
			if(idcard == ""){ 
				$('#idcartHelp').text("กรุณาระบุข้อมูล");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				fieldsume();
				return false;
			}
			
			if(idcard.length != 17){ 
				$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				fieldsume();
				return false;
			}

			//var num = str_split(idcard); // function เพิ่มเติม
            var num = idcard.split("-");
            console.log(num);
            
			var id ="";
			for (i = 0; i < num.length; i++) {
				id += num[i];
			}

			var arr = [];
			for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}

			var sum = 0;
			var total = 0;
			var digi = 13;

			for(i=0;i<12;i++){
				sum = sum + (arr[i] * digi);
				digi--;
			}

			total = ((11 - (sum % 11)) % 10);
			
			if(total == arr[12]){    
				$("#idcart").removeClass("is-invalid");
				if(idcartlog > 0){--idcartlog;}
				
				//call api 
				/*var CORS = 'https://cors-anywhere.herokuapp.com/';
				$.ajax({
					url: CORS + 'https://passwordwolf.com/api/',
					dataType: "json",
					type: "GET",
					data: {
						length: 10,
						upper: "off",
						lower: "off",
						special: "off",
						exclude: "012345",
						repeat: 5
					},
					success: function(jsonObject, status) {
						console.log("ajax result: " + JSON.stringify(jsonObject));
					}
				});*/

				return true;

			}else{   
				
				$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				fieldsume();
				return false;
			}
		}else{
			$('#idcartHelp').text("กรุณาระบุข้อมูล");
			$("#idcart").addClass("is-invalid");
			if(idcartlog == 0){idcartlog++;}
		}


		/*var val = $("#idcart").val();
		if( val != null && val != "" ){
			$('#idcartHelp').text("");

			var res = val.split("-");
			var id ="";
			for (i = 0; i < res.length; i++) {
				id += res[i];
			}

			if(id.length < 13){
				$('#idcartHelp').text("หมายเลขบัตรปรชาชนของคูณไม่กูกต้อง");
				if(idcartlog == 0){idcartlog++;}
				fieldsume();
			}else{

				var arr = [];
				for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}
				var strarr=[];
				for (i = 0; i < arr.length; i++) {
					var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
					if(!numberRegex.test(arr[i])) {
						strarr.push(arr[i]);
					} 
				}

				if(strarr.length > 0){
					$("#idcart").prop('required',true);
					$('#idcartHelp').text("หมายเลขบัตรปรชาชนของคูณไม่กูกต้อง");
					if(idcartlog == 0){idcartlog++;}
					fieldsume();
				}else{
					if(idcartlog > 0){--idcartlog;}
					fieldsume();
					if($('#condition').prop("checked") && fieldsume()== 0 ){
						$('#btn-submit').attr("disabled",false);
					}

					//call api 
					var CORS = 'https://cors-anywhere.herokuapp.com/';

					$.ajax({
					url: CORS + 'https://passwordwolf.com/api/',
					dataType: "json",
					type: "GET",
					data: {
						length: 10,
						upper: "off",
						lower: "off",
						special: "off",
						exclude: "012345",
						repeat: 5
					},
					success: function(jsonObject, status) {
						console.log("ajax result: " + JSON.stringify(jsonObject));
					}
					});


					//เช็คหมายเลขบัตรปรชาชน   ?????
				}
			}
		}else{
			$('#idcartHelp').text("กรุณากรอก เลขบัตรปรชาชน");
			//$("fname").addClass("form-require");
			if(idcartlog == 0){idcartlog++;}
			fieldsume();
		}*/
	});

	var technidStatus =true;
	$('#technid').keyup(function(e) {
        var dInput = this.value;
		if (dInput != null && dInput != "") {
			//$('#btn-submit').attr("disabled",true);
		}
		else{
			$('#technidHelp').text("");
			$("#technid").removeClass("is-invalid");

			$('#technidCode').text("");
			$('#technidName').text("");
			$("#collapseTechnid").collapse("hide");
			//$('#btn-submit').attr("disabled",false);
        }

        /*$('#technidHelp').text("");
			$("#technid").removeClass("is-invalid");

			$('#technidCode').text("");
			$('#technidName').text("");
			$("#collapseTechnid").collapse("hide");
			$('#btn-submit').attr("disabled",false);*/
	});
	

	/*$("#technid").change(function(){ 
		var val = $("#technid").val();
		if(val != null && val != ""){
			$('#technidHelp').text("");
			
			$.ajax({
				url: "<?php echo site_url('form/ajaxTechn');?>", //ทำงานกับไฟล์นี้
				data:  {
					'techn_cod':val
				},
				type: "POST",
				dataType: 'json',
				cache: false,
				//async:false,
				success: function(data, status) {
					if(data == false){
						$("#technid").addClass("is-invalid");
						$('#technidHelp').removeClass("text-muted-y");
						$('#technidHelp').addClass("text-muted");
						$('#technidHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
						if(technidlog == 0){technidlog++;}
						fieldsume();
					}else{
						$('#technidHelp').text("");
						$("#technid").removeClass("is-invalid");
						if(technidlog > 0){--technidlog;}
						if($('#condition').prop("checked") && fieldsume()== 0 ){
							$('#btn-submit').attr("disabled",false);
						}else{
							//$('#btn-submit').attr("disabled",true);
						}
					}
				},
				error: function(xhr, status, exception) { 
					//console.log(status);
				}
			});
		}else{
			$("#technid").removeClass("is-invalid");
			$('#technidHelp').text("");
			
			if(technidlog > 0){--technidlog;}
			if($('#condition').prop("checked") && fieldsume()== 0 ){
				$('#btn-submit').attr("disabled",false);
			}else{
				//$('#btn-submit').attr("disabled",true);
			}
		}
	});
	*/

	//$("#collapseTechnid").collapse("hide");
	
	$("#btn-cancel").click(function(){
		$('#cancelModal').modal('show'); 
	});

	$(".btn-cancel").click(function(){ 
		$("#prefix-select").val(0);
		$("#prefix").val(null);
		$("#fname").val(null);
		$("#sname").val(null);
		$("#email").val(null);
		$("#tel").val(null);
		$("#address").val(null);
		$("#province").val(null);
		$("#amphue").val(null);
		$("#district").val(null);
		$("#zipcode").val(null);
		$("#idcart").val(null);
		$("#technid").val(null);
		//$('#condition').prop("checked",false);

        $('#technidHelp').text("");
        $("#btn-technid").removeClass("is-invalid");
        $("#technid").removeClass("is-invalid");

		$('#technidCode').text("");
		$('#technidName').text("");
		$("#collapseTechnid").collapse("hide");
		$('#btn-submit').attr("disabled",false);
	});

	
	$("#SaveModal").click(function(){ 
		$('#technidCode').text("");
		$('#technidName').text("");
		$("#collapseTechnid").collapse("hide");
	});

	$("#btn-fails").click(function(){ 
		if (technidStatus == false) {
			$("#technid").val(null);
		}
	});

	$(".number-type").keypress(function (e) {
		if (/\d+|[/b]+/i.test(e.key) ){
			//console.log("character accepted: " + e.key)
		} else {
			//console.log("illegal character detected: "+ e.key)
		return false;
		}
	});

	function fieldsume(){ 
		intlog =  prefixlog + fnamelog + snamelog + addresslog + emaillog + tellog+ provincelog +amphuelog +zipcodelog +districtlog +idcartlog +technidlog+conditionlog;
		return intlog;
	}

    function Tel_format(tel){
		var res =  false;
		var testEmail = /^[0-9]+-([0-9])+-[0-9]{2,4}$/i;
		if (testEmail.test(tel)){
			res = true;
		}else{
			res = false;
		}
		return res;
    }

	function Email_format(email){
		var res =  false;
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (testEmail.test(email)){
			res = true;
		}else{
			res = false;
		}
		return res;
    }
    


    /////////////////// Condition page ////////////////////////
    var conditionStatus = false;
	
	$("#condition").change(function(){ 
		if($('#condition').prop("checked")){
			conditionStatus = true;
		}else{
			conditionStatus = false;
		}
	});

	
