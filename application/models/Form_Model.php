<?php
class Form_Model extends CI_Model {

	  function __construct() { 
	     parent::__construct(); 
	  } 
	  
	  /*public function select($search = null){
		$this->db->select('*');
	    $this->db->from('news');
 		if($search != null){$this->db->like('topic', $search, 'both'); }
		$query = $this->db->get();
			

	    $data = $query->result(); 
	    return $data;
	  }*/

	  public function idcart($search=array()){
	    /*$this->db->select('*');
		$this->db->from('dbo.nbtc_users');
 		//if($search != null){$this->db->where('idcart', $search); }
		//$this->db->where('idcart',$search);
		$query = $this->db->get();
		$data = $query->result(); 
		return $search;*/

		$response = array();
		if(isset($search['idcart']) ){
			// Select record
			$this->db->select('*');
			$this->db->from('dbo.nbtc_users');
			$this->db->where('idcart', $search['idcart']);
			$records = $this->db->get();
			$response = $records->result_array();
		}
		return $response;

	  }

	  public function province($postData=array()){
		$Query = "select * from provinces order by province_name";
		$Res= $this->db->query($Query);
		$data = $Res->result();

		
	    /*$this->db->select('*');
		$this->db->from('dbo.provinces');
		$this->db->order_by("province_name", "asc");
		$query = $this->db->get();
		$data = $query->result(); */
		return $data;
	  }

	  public function amphoe($postData=array()){
		$response = array();
		if(isset($postData['province_cod']) ){
			$Query = "select * from amphurs where provinces_id = ".$postData['province_cod']." and amphur_name not like '%*%' order by amphur_name";
			$Res= $this->db->query($Query);
			$response = $Res->result();
			// Select record
			/*$this->db->select('*');
			$this->db->from('dbo.amphurs');
			$this->db->order_by("amphur_name", "asc");
			$this->db->where('provinces_id', $postData['province_cod']);
			$records = $this->db->get();
			$response = $records->result_array();*/
		}
		return $response;
	  }

	  public function districs($DataAmphure=array()){
		 $response = array();
		if(isset($DataAmphure['amphue_cod']) ){
			$Query = "select * from districts where amphurs_id = ".$DataAmphure['amphue_cod']." and district_name not like '%*%' order by district_name";
			$Res= $this->db->query($Query);
			$response = $Res->result();

			/*$this->db->select('*');
			$this->db->from('dbo.districts');
			$this->db->where('amphurs_id', $DataAmphure['amphue_cod']);
			$this->db->order_by("district_name", "asc");
			$records = $this->db->get();
			$response = $records->result_array();*/
		}
		return $response;
	  }

	  public function zipcode($province,$amphue,$district){
	    
		   // Select record
		   $this->db->select('*');
		   $this->db->from('dbo.zipcode');
		   $this->db->where('provinces_id', $province);
		   $this->db->where('amphurs_id', $amphue);
		   $this->db->where('districts_id', $district);
		   $records = $this->db->get();
		   $response = $records->result();
	   
		   return $response;
	  }
	  public function insert($data) { 
		
		//return $this->db->insert("nbtc_users", $data); 
       	if ($this->db->insert("nbtc_users", $data)) { 
            return true; 
        }
      } 

      /*public function delete($id) { 
         if ($this->db->delete("news", "id = ".$id)) { 
            return true; 
         } 
      } 
   
      public function update($data,$id) { 
      	
         $this->db->set($data); 
         $this->db->where("id", $id); 
         $this->db->update("news", $data); 
      } */


}

?>