<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Form extends CI_Controller {
  private $seminar_conn;
  function __construct() { 
     
      parent::__construct(); 
     
      //เรียกใช้งาน Class helper 
      $this->load->library('session');    
      $this->load->helper('url'); 
      $this->load->helper('form');
     
      //เรียกใช้งาน Class database     
      $this->load->database(); 

      //เรียกใช้งาน Model     
      $this->load->model('Form_Model');
       
       // load db ( agent )
       $this->load->config('internaldb');
       $this->db_config = $this->config->item('agent_db');
       $this->connectSeminarDB();
  } 

	public function index()
	{
    $data['province'] = $this->Form_Model->province();
		//$this->load->view('form',$data);
		$this->load->view('condition',$data);
  }

  public function condition()
	{
    $data['province'] = $this->Form_Model->province();
		$this->load->view('form',$data);
  }

  public function ajaxIdcart($idcart = null)
	{
    $postData = $this->input->post();
    $data = $this->Form_Model->idcart($postData);
    echo json_encode($data);
  }

  public function ajaxProvinces($province_cod = null)
	{
    $postData = $this->input->post();
    $data = $this->Form_Model->amphoe($postData);
    echo json_encode($data);
  }

  public function ajaxAmphue($amphue_cod = null)
	{
    $DataAmphure = $this->input->post();
    $data = $this->Form_Model->districs($DataAmphure);
    echo json_encode($data);
  }

  public function ajaxDistricts($province = null,$amphue = null,$district = null)
	{
    $province = $this->input->post('province');
    $amphue = $this->input->post('amphue');
    $district = $this->input->post('district');

    $data = $this->Form_Model->zipcode($province,$amphue,$district);
    echo json_encode($data);
  }

  public function ajaxTechn()
	{
    $technId = $this->input->post('techn_cod');
    $ress;

    $query = "select AgentCode,AgentName,AgentSurName from Agent where (AgentCode ='".$technId."')";
    $result = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    if(sqlsrv_num_rows($result) > 0){
      while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
        $ress = $row;
      }
    }else{
      $ress = false;
    }

    echo json_encode($ress);
  }
  
  public function insert(){

    $data = array( 
      'prefix' => $this->input->post('prefix'), 
        'fname' => $this->input->post('fname'), 
        'sname' => $this->input->post('sname'), 
        'email' => $this->input->post('email'),
        'tel' => $this->input->post('tel'),  
        'address' => $this->input->post('address'), 
        'district_id' => $this->input->post('district'), 
        'amphur_code' => $this->input->post('amphue'), 
        'province_id' => $this->input->post('province'), 
        'zipcode' => $this->input->post('zipcode'), 
        'idcart' => $this->input->post('idcart'), 
        'technid' => $this->input->post('technid'),
        //'condition' => $this->input->post('condition'),
        'condition' => 1,
        'cdate'=>date("Y-m-d H:m:s")
     );

     $returns = $this->Form_Model->insert($data); 
     echo json_encode($returns );
     //print_r($data);
     //exit();
    //$returns = $this->Form_Model->insert($data); 
    //echo json_encode($returns);
  }


  private function connectSeminarDB(){
			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);
            
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}
  }
  
  public function closeConnectSeminarDB(){
    sqlsrv_close( $this->seminar_conn );
  }
   

}
