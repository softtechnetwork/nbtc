<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Form extends CI_Controller {
  private $seminar_conn;
  function __construct() { 
     
      parent::__construct(); 
    
     
      //เรียกใช้งาน Class helper 
      $this->load->library('session');    
      $this->load->helper('url'); 
      $this->load->helper('form');
     
      //เรียกใช้งาน Class database     
      $this->load->database(); 

      //เรียกใช้งาน Model     
      $this->load->model('Form_Model');
       
       // load db ( agent )
       $this->load->config('internaldb');
       $this->db_config = $this->config->item('agent_db');
       $this->connectSeminarDB();
      
       
  } 

	public function index()
	{
    
    $data['districts'] = $this->Form_Model->districts();
    
    /*$data['amphurs'] = $this->Form_Model->amphurs();
    $data['provinces'] = $this->Form_Model->provinces();
    $data['zipcode'] = $this->Form_Model->zipcode();*/
    $this->checkAgent();
		$this->load->view('form',$data);
	}
  public function checkAgent(){
    // DB ช่าง
    $query = "select top 1 AgentCode,AgentName,AgentSurName from Agent";
    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    if(sqlsrv_num_rows($stmt) > 0){
        echo '===================== DB ช่าง  =====================';
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC )) {
        
            // echo '<PRE>';
             print_r($row);
            // exit();
      }
   
    }


  }
  public function insert(){

    $data = array( 
        
        'fname' => $this->input->post('fname'), 
        'sname' => $this->input->post('sname'), 
        'address' => $this->input->post('address'), 
        'district' => $this->input->post('district'), 
        'amphoe' => $this->input->post('amphoe'), 
        'province' => $this->input->post('province'), 
        'zipcode' => $this->input->post('zipcode'), 
        'idcart' => $this->input->post('idcart'), 
        'technid' => $this->input->post('technid')
        //'cdate'=>date("Y-m-d H:m:s")
     );
     $this->Form_Model->insert($data); 
  }


  private function connectSeminarDB(){
       
			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);
            
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}
     


  }
  
  public function closeConnectSeminarDB(){
    sqlsrv_close( $this->seminar_conn );
  }
   

}
