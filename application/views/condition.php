<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome</title>
	<link rel="stylesheet" href="<?php echo base_url('./assete/css/bootstrap.min.css');?>">
	<!--<link rel="stylesheet" href="<?php echo base_url('./assete/font/font.css');?>">-->
	<link rel="stylesheet" href="<?php echo base_url('./assete/font/Prompt/font.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('./assete/font/font.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('./assete/css/css.css');?>">
	<script src="<?php echo base_url('./assete/js/jquery.js');?>"></script> 
	<script src="<?php //echo base_url('./assete/js/bootstrap.js');?>"></script>  
	<script src="<?php echo base_url('./assete/js/bootstrap.min.js');?>"></script> 	
	<!--<script src="<?php// echo base_url('./assete/js/js.js');?>"></script> -->
</head>
<body>

<div class="container-fluid">
	<div class="row" style=" margin-top: 25px;text-align: center;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<img style="width: 93px;" src="<?php echo site_url('./img/psi_logo_500x500px.png');?>" alt="..." class="img-circle">	
			<img style="width: 93px;" src="<?php echo site_url('./img/nbtc_logo.png');?>" alt="..." class="img-circle">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-box" style=" padding: 0 13px;">
			<label id="headerCondition" style=" color: #383b3f; font-family: Prompt-Medium">ข้อกําหนด เงื่อนไข และความยินยอมสำหรับผู้ที่เข้าร่วมโครงการรับสิทธิเยียวยาการเปลี่ยนหัวรับสัญญาณดาวเทียม หรือ LNB</label>
		</div>
	</div>
</div>
<div class="container" >

	
	<!--<div class="row ">-->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form shadow-sm form-box">	


				<div class="row" style=" font-family:KrungthaiFast">
					<div class="col-12 ">
					<ol style=" padding-left: 0px; list-style-type: none;">
					<li style=" color: #383b3f; font-family: Prompt-Medium">1. สาระสำคัญของโครงการรับสิทธิเยียวยาการเปลี่ยนหัวรับสัญญาณดาวเทียม หรือ LNB (โครงการฯ)</li>
						1.1 ช่วงระยะเวลาเปิดลงทะเบียนเข้าร่วมโครงการ คือ ตั้งแต่วันที่ 1 กุมภาพันธ์ 2564 ถึงวันที่ 28 กุมภาพันธ์ 2564<br>
						1.2 ผู้เข้าร่วมโครงการฯ เป็นผู้ได้รับผลกระทบจากปัญหาคลื่นความถี่ 5G ไปรบกวนสัญญาณทีวีดาวเทียมจานดำ C-band
						ทำให้ไม่สามารถรับชมโทรทัศน์ผ่านดาวเทียมตามปกติได้<br>
						1.3 เพื่อเป็นข้อมูลส่งให้ คณะกรรมการกิจการกระจายเสียง กิจการโทรทัศน์ และกิจการโทรคมนาคมแห่งชาติ (กสทช.)<br>
						1.4 เพื่อเป็นข้อมูลสำหรับ กสทช. พิจารณารับสิทธิเยียวยาการเปลี่ยนหัวรับสัญญาณดาวเทียม หรือ LNB<br>
						<li style=" color: #383b3f; font-family: Prompt-Medium">2. ข้อความตกลงยินยอมของผู้เข้าร่วมโครงการฯ</li>
					* "ข้าพเจ้า" หมายถึง บุคคลผู้เข้าร่วมโครงการฯ<br>
						2.1 ข้าพเจ้าตกลงยินยอมให้ บริษัท พีเอสไอ คอร์ปอเรชั่น จำกัด และ กสทช. จัดเก็บและประมวลผลข้อมูลส่วนบุคคล
						ที่ข้าพเจ้าได้ให้ไว้ในการลงทะเบียนเข้าร่วมโครงการฯ<br>
						2.2 ข้าพเจ้าตกลงยินยอมให้ บริษัท พีเอสไอ คอร์ปอเรชั่น จำกัด และ กสทช. จัดเก็บ ประมวลผล และเปิดเผยข้อมูล
						ส่วนบุคคลของข้าพเจ้าต่อ กสทช. และผู้รับให้บริการที่ได้รับมอบหมายจาก กสทช. เพื่อการประมวลผลและการตรวจสอบข้อมูล และ/หรือเพื่อการยืนยันตัวตน หรือ
						เพื่อการตรวจสอบคุณสมบัติเพื่อรับสิทธิตามโครงการฯ และ/หรือเพื่อการบริหารจัดการโครงการฯ<br>
						2.3 ข้าพเจ้าตงลงยินยอมให้ บริษัท พีเอสไอ คอร์ปอเรชั่น จำกัด และ กสทช. จัดเก็บ ประมวลผล และเปิดเผยข้อมูลส่วนบุคคลของข้าพเจ้า เพื่อประโยชน์
						ในการดำเนินการมาตรการอื่นของ กสทช. ในอนาคต<br>
						2.4 ข้าพเจ้าตงลงยินยอมให้หน่วยงานของรัฐที่เกี่ยวข้องร้องขอ สอบถาม และใช้ข้อมูลส่วนบุคคลของข้าพเจ้า เพื่อประโยชน์ในการดำเนินโครงการฯ<br>
						2.5 ข้าพเจ้ารับทราบว่าการนำเข้าสู่ระบบคอมพิวเตอร์ซึ่งข้อมูลอันเป็นเท็จเป็นการกระทำความผิดตามกฎหมาย<br>
					</ol>
					<p  style=" color: #383b3f; font-family: Prompt-Medium">
					ข้าพเจ้าได้อ่าน รับทราบ และตกลงยินยอมปฏิบัติตามหลักเกณฑ์และเงื่อนไขของโครงการฯ<br>
					ข้าพเจ้าขอรับรองว่าข้อมูลที่ข้าพเจ้าได้ให้ไว้ในการลงทะเบียนเข้าร่วมโครงการฯ ถูกต้องตามความเป็นจริงทุกประการ<br>
					</p>

					<div class="px-md-5" style="text-align: center;">
						<div class="col-12">
						<div class="form-group form-check">
							<input class="" type="checkbox" name="condition" id="condition">
							<label class="form-check-label" for="condition">ข้าพเจ้าได้อ่าน และตกลงยินยอมตามรายละเอียดข้อตกลงและความยินยอมข้างต้น
							</label>
							</div>
						</div>
						<div class="col-12">
							<button id="confirm-btn" type="button" class="btn btn-primary"><strong>ยืนยัน</strong></button>
						</div>
						
						<!--<div class="col-12" id="back-box">
							<button id="back-btn" type="button" class="btn btn-link"><strong>กลับสู่หน้าหลัก</strong></button>
						</div>-->
						<div class="col-12">
							<strong style="font-size: 13px; color: #dc3545!important;">**หมายเหตุ ผู้ลงทะเบียนรับทราบว่า หากผู้ลงทะเบียนไม่ตกลงและยอมรับตามข้อตกลง<br>และเงื่อนไขข้างต้นจะไม่สามารถลงทะเบียนได้</strong>
						</div>
					</div>

					
					</div>
				</div>

		</div>
	<!--</div>-->
</div>

<!-- Save Modal -->
<div class="container" >
	<div class="modal fade SaveModal" id="ConditionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		<div class="modal-body">
			<div class="col-12" style="padding-bottom:13px;">
				<img style="width: 93px;" src="<?php echo site_url('./img/warnning.png');?>" alt="..." class="img-circle">
			</div>
			<div class="col-12">กรุณาคลิก [/] เพื่อตกลง และยินยอมรายละเอียด ก่อนกดปุ่มยืนยัน</div>
			<div class="col-12"><button type="button" class="btn btn-primary" data-dismiss="modal" style="  background-color: #3275bd; font-weight: 700;">ตกลง</button></div>
		</div>
		</div>
	</div>
	</div>
</div>


</body>
</html>
<script>
	//$("#SaveFailModal").modal("show");
	/*var conditionStatus = false;
	console.log($('#condition').prop("checked"));
	$("#condition").change(function(){ 
		if($('#condition').prop("checked")){
			conditionStatus = true;
		}else{
			conditionStatus = false;
		}
	});*/
	
	$("#confirm-btn").click(function(){ 
		if (!$('#condition').prop("checked")) {
			$("#ConditionModal").modal("show");
			$("#technid").val(null);
		}else{
			window.location.href = "<?php echo site_url().'form/condition';?>";
			$('#condition').prop("checked",false);
		}
	});
	

	</script>
	<!--<script src="<?php //echo base_url('./assete/js/js.js');?>"></script> -->