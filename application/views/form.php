<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Welcome</title>
	<link rel="stylesheet" href="<?php echo base_url('./assete/css/bootstrap.min.css');?>">
	<!--<link rel="stylesheet" href="<?php echo base_url('./assete/font/font.css');?>">-->
	<link rel="stylesheet" href="<?php echo base_url('./assete/font/Prompt/font.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('./assete/font/font.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('./assete/css/css.css');?>">
	<script src="<?php echo base_url('./assete/js/jquery.js');?>"></script> 
	<!--<script src="<?php //echo base_url('./assete/js/bootstrap.js');?>"></script>  -->
	<script src="<?php echo base_url('./assete/js/bootstrap.min.js');?>"></script> 
	
</head>
<body>
<style type="text/css">

		.is-invalid {
			/*background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='%23dc3545' viewBox='0 0 12 12'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e);*/
			background-image:inherit !important;
			
		}

		.form-control .is-invalid {
			border-color: #dc3545 !important;
			padding-right: inherit !important;
			background-repeat: no-repeat;
			background-position: right calc(.375em + .1875rem) center;
			background-size: calc(.75em + .375rem) calc(.75em + .375rem);
		}
		
		.focusedInput {
			box-shadow: 0 0 8px rgba(82, 168, 236, 0) !important;
			-moz-box-shadow: 0 0 8px rgba(82, 168, 236, 0) ;
		}
		
		/*.form-control .is-inval  {
			border-color: #dc3545 !important;
			padding-right: inherit !important;
			background-repeat: no-repeat;
			background-position: right calc(.375em + .1875rem) center;
			background-size: calc(.75em + .375rem) calc(.75em + .375rem);
		}*/
	</style>
</style>

<div class="container">
	<div class="row" style="     margin-top: 25px;text-align: center;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<img style="width: 93px;" src="<?php echo site_url('./img/psi_logo_500x500px.png');?>" alt="..." class="img-circle">	
			<img style="width: 93px;  padding-left: 16px;" src="<?php echo site_url('./img/nbtc_logo.png');?>" alt="..." class="img-circle">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-box" style=" padding: 0 13px;">
			<h1 style=" color: #383b3f; font-family: Prompt-Medium">ลงทะเบียนโครงการ</h1>
			<p style="color: #6c757d !important; font-family: KrungthaiFast;">กรุณากรอกข้อมูลของท่านให้ถูกต้องครบถ้วน เพื่อใช้ในการลงทะเบียน </p>
			<!--<label>ผู้สมัครสมาชิกตกลงยินยอมผูกพันตามข้อกําหนดและเงื่อนไขการใช้บริการร้องเรียนออนไลน์ของ สํานักงานคณะกรรมการข้อมูลข่าวสารของราชการ</label>-->
		</div>
	</div>
</div>
<div class="container" >
	<!--<div class="row ">-->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form shadow-sm form-box">	
			<!--<form method="post" action="<?php  echo"#";// echo  site_url('form/insert');?>">-->
				<div class="row">
					<div class="col-xs-12  col-sm-12 col-md-12 col-lg-2 ">
						<div class="form-group">
							<label class="label-name" for="prefix-select">คำนำหน้าชื่อ <span class="requird-icon">*</span></label>
							<select class="form-control" name="prefix-select"  id="prefix-select" data-live-search="true">
								<option value="0" selected='false' disabled>โปรดเลือก</option>	
								<option value="1" >นาย</option>	
								<option value="2" >นาง</option>	
								<option value="3" >นางสาว</option>	
								<option value="4" >อื่นๆ</option>
							</select>
							<small id="prefix-selectHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-xs-12  col-sm-12 col-md-12 col-lg-2" id="prefix-box">
						<div class="form-group">
							<label class="label-name" for="prefix">โปรดระบุคำนำหน้า <span class="requird-icon">* </span></label>
							<input type="text" class="form-control" id="prefix" name="prefix" aria-describedby="prefixHelp" value=""  placeholder="คำนำหน้า">
							<small id="prefixHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-xs-12  col-sm-12 col-md-12 col-lg-5" id="fname-box">
						<div class="form-group">
							<label class="label-name" for="fname">ชื่อ <span class="requird-icon">* <small style=" color: #dc3545!important;font-weight: 600;"> ไม่ต้องระบุคำนำหน้าชื่อ (เช่น นาย, นาง, นางสาว)</small></span></label>
							<input type="text" class="form-control" id="fname" name="fname" aria-describedby="fnameHelp" value="<?php echo $this->session->userdata('fname'); ?>"  placeholder="ชื่อ">
							<small id="fnameHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5" id="sname-box">
						<div class="form-group">
							<label class="label-name" for="sname">นามสกุล <span class="requird-icon">*</span></label>
							<input type="text" class="form-control" id="sname" name="sname" aria-describedby="snameHelp" value="<?php echo $this->session->userdata('sname'); ?>"  placeholder="นามสกุล">
							<small id="snameHelp" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12  col-lg-6">
						<div class="form-group">
							<label class="label-name" for="email">อีเมล์ (ถ้ามี) ให้ระบุได้เพียง 1 อีเมล์</label>
							<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?php echo $this->session->userdata('email'); ?>"  placeholder="อีเมล์">
							<small id="emailHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12  col-lg-6">
						<div class="form-group">
							<label class="label-name" for="tel">เบอร์โทรศัพท์มือถือที่ติดต่อได้ <span class="requird-icon">*</span></label>
							<input type="tel" maxlength="10" class="form-control number-type" pattern="\d*" id="tel" name="tel" aria-describedby="telHelp" value="<?php echo $this->session->userdata('tel'); ?>"  placeholder="เบอร์โทรศัพท์มือถือที่ติดต่อได้">
							<small id="telHelp" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label class="label-name" for="address">ที่อยู่ <span class="requird-icon">*</span></label>
							<textarea class="form-control" id="address" name="address" aria-describedby="addressHelp"  placeholder="ที่อยู่"><?php echo $this->session->userdata('address'); ?></textarea>
							<small id="addressHelp" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-12  col-md-12 col-lg-6">
						<div class="form-group">
							<label class="label-name" for="province">จังหวัด <span class="requird-icon">*</span></label>
							<select class="form-control" name="province"  id="province" data-live-search="true">
							<option value="" selected='false' disabled>จังหวัด</option>	
							<?php foreach ($province as $ara) : ?>
								<option value="<?php echo $ara->id; ?>"><?php echo $ara->province_name; ?></option>
								<?php endforeach ?>
							</select>
						    <small id="provinceHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-12 col-sm-12  col-md-12 col-lg-6 ">
						<div class="form-group">
							<label class="label-name" for="amphue">อำเภอ / เขต <span class="requird-icon">*</span></label>
							<select class="form-control" name="amphue"  id="amphue">
								<option value="">อำเภอ / เขต</option>
							</select>
						    <small id="amphueHelp" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12  col-sm-12  col-md-12 col-lg-6">
						<div class="form-group">
							<label class="label-name" for="district">ตำบล / แขวง <span class="requird-icon">*</span></label>
							<select class="form-control" name="district"  id="district">
								<option value="">ตำบล / แขวง</option>
							</select>
						    <small id="districtHelp" class="form-text text-muted"></small>
						</div>
					</div>
					<div class="col-12  col-sm-12 col-md-12 col-lg-6">
						<div class="form-group">
							<label class="label-name no-spin" for="district">รหัสไปรษณีย์ <span class="requird-icon">*</span></label>
						    <input type="tel" maxlength="5" class="form-control no-spin number-type" pattern="\d*" name="zipcode" id="zipcode" aria-describedby="zipcodeHelp" placeholder="รหัสไปรษณีย์">
						    <small id="zipcodeHelp" class="form-text text-muted"></small>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label class="label-name" for="idcart">เลขบัตรประชาชน <span class="requird-icon">*</span></label>
							<input class="form-control no-spin number-type" pattern="\d*"  name="idcart" type="tel" maxlength="13" id="idcart" aria-describedby="idcartHelp" placeholder="เลขบัตรประชาชน" > 
							<small id="idcartHelp" class="form-text text-muted"></small>
						</div>
					</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group" style="margin-bottom: 0rem;">
								<span class="label-name" for="technid">รหัสช่าง <small style=" color: #dc3545!important;font-weight: 600;">* หากท่านเป็นช่าง PSI สามารถระบุรหัสช่างเพื่อความสะดวกในการติดต่อกลับ</small></ </span>
								<div class="row">
									<div class="col-6 col-sm-7 col-md-7 col-lg-9">
										<input type="text" class="form-control" name="technid" id="technid" aria-describedby="technidHelp"  placeholder="รหัสช่าง">
										<small id="technidHelp" class="form-text text-muted"></small>
									</div>
									<div class="col-6 col-sm-5 col-md-5 col-lg-3">
										<div class="form-group">
											<button type="submit" id="btn-technid" class="btn btn-techn"style=" ">ตรวจสอบรหัสช่าง</button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-12 ">
										<div class="collapse" id="collapseTechnid" style="margin: -15px 0px 15px 0;">
											<div class="card card-body">
												<span id="technidCode"></span>
												<span id="technidName" ></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--<div class="row">
						<div class="col-12">
							<div class="form-check">
								<input class="" type="checkbox" name="condition" id="condition">
								<label class="form-check-label" for="condition">
									<a id="btn-condition" href="<?php echo "#" ;?>">โปรดอ่านข้อกำหนดและเงื่อนไขก่อนลงทะเบียน</a>
								</label>
							</div>
						</div>
					</div>-->
					<div class="row">
						<div class="col-6">
							<button type="submit" id="btn-cancel" class="btn btn-link shadow-sm btn-submit form-group">ยกเลิก</button>
						</div>
						<div class="col-6">
							<button type="submit" id="btn-submit" class="btn btn-primary shadow-sm btn-submit form-group">ลงทะเบียน</button>
						</div>
					</div>
			<!--</form>-->
		</div>
	<!--</div>-->
</div>

<!-- Save Modal -->
<div class="container" >
	<div class="modal fade SaveModal" id="SaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
			<div class="modal-body">
				<div class="col-12" style="padding-bottom:13px;">
					<img style="width: 93px;" src="<?php echo site_url('./img/thangyou.png');?>" alt="..." class="img-circle">
				</div>
				<div class="col-12">ขอบคุณสำหรับการลงทะเบียน</div>
				<div class="col-12"><button id="SaveModal-btn" type="button" class="btn btn-primary" data-dismiss="modal">ตกลง</button></div>
			</div>
			</div>
		</div>
	</div>
</div>

<!--Save fail Modal -->
<div class="container" >
	<div class="modal fade SaveModal" id="SaveFailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="col-12" style="padding-bottom:13px;">
						<img style="width: 93px;" src="<?php echo site_url('./img/close.png');?>" alt="..." class="img-circle">
					</div>
					<div class=""> <h2 style=" color: #383b3f; font-family: Prompt-Medium">ข้อมูลบางอย่างไม่ครบถ้วน หรือไม่ถูกต้อง</h2> </div>
					<div class="" style="font-size: 1rem !important;"> <span>กรุณาตรวจสอบข้อมูลของท่าน และลองใหม่อีกครั้ง</span></div>
					<div class="">
						<div class="col-12">
							<button type="button" class="btn btn-fails" data-dismiss="modal">ตกลง</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- cancel Modal -->
<div class="container" >
	<div class="modal fade cancelModal" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="col-12" style="padding-bottom:13px;">
						<img style="width: 93px;" src="<?php echo site_url('./img/warnning.png');?>" alt="..." class="img-circle">
					</div>
					<div class="col-12"> <h2>ต้องการยกเลิกการกรอกข้อมูลหรือไม่</h2> </div>
					<div class="col-12" style="font-size: 1rem !important;"> <span>หากยกเลิก ท่านต้องกรอกข้อมูลใหม่ทั้งหมด</span></div>
					<div class="row">
						<div class="col-6">
							<button type="button" class="btn btn-cancel" data-dismiss="modal">ยกเลิกการกรอกข้อมูล</button>
						</div>
						<div class="col-6">
							<button type="button" class="btn btn-continue" data-dismiss="modal">กรอกข้อมูลต่อ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



</body>
</html>
<script>
	//$("#SaveFailModal").modal("show");

	//$("#SaveModal").modal("show");
	//$("#cancelModal").modal("show");

	var intlog = 0;
	var prefixlog = 0;
	var fnamelog = 0;
	var snamelog = 0;
	var addresslog = 0;
    var emaillog = 0;
    var tellog = 0;
	var provincelog = 0;
	var amphuelog = 0;
	var zipcodelog = 0;
	var districtlog = 0;
	var idcartlog = 0;
	var technidlog = 0;
	var conditionlog = 0;
	
	$("#collapseTechnid").collapse("show");

	$("#prefix-box").hide();
	$("#prefix-select").change(function(){ 
		if($(this).val() != null && $(this).val() != ""){
			switch($(this).val()){
				case '1':
					$("#prefix-box").hide();
					$("#prefix").val('นาย');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '2':
					$("#prefix-box").hide();
					$("#prefix").val('นาง');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '3':
					$("#prefix-box").hide();
					$("#prefix").val('นางสาว');
					$("#fname-box").removeClass("col-lg-4");
					$("#sname-box").removeClass("col-lg-4");
					$("#fname-box").addClass("col-lg-5");
					$("#sname-box").addClass("col-lg-5");
					if(prefixlog > 0){--prefixlog;}
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
				break;
				case '4':
					$("#prefix").val('');
					$("#fname-box").removeClass("col-lg-5");
					$("#sname-box").removeClass("col-lg-5");
					$("#prefix-box").show();
					$("#fname-box").addClass("col-lg-4");
					$("#sname-box").addClass("col-lg-4");
					
					$('#prefix-selectHelp').text("");
					$("#prefix-select").removeClass("is-invalid");
					
				break;
			}
		}else{
			$('#prefix-selectHelp').text("กรุณาระบุข้อมูล");
			$("#prefix-select").addClass("is-invalid");
		}
	});

	$("#prefix").change(function(){ 
		if($("#prefix").val() != null && $("#prefix").val() != ""){
			$('#prefixHelp').text("");
			$("#prefix").removeClass("is-invalid");
			if(prefixlog > 0){--prefixlog;}
		}else{
			$("#prefix").addClass("is-invalid");
			$('#prefixHelp').text("กรุณาระบุข้อมูล");
			if(prefixlog == 0){prefixlog++;}
		}
	});
	 
	$("#fname").change(function(){ 
		if($("#fname").val() != null && $("#fname").val() != ""){
			$('#fnameHelp').text("");
			$("#fname").removeClass("is-invalid");
			if(fnamelog > 0){--fnamelog;}
		}else{
			$('#fnameHelp').text("กรุณาระบุข้อมูล");
			if(fnamelog == 0){fnamelog++;}
			$("#fname").addClass("is-invalid");
		}
	});
	
	$("#sname").change(function(){ 
		if($("#sname").val() != null && $("#sname").val() != ""){
			$('#snameHelp').text("");
			$("#sname").removeClass("is-invalid");
			if(snamelog > 0){--snamelog;}
		}else{
			$('#snameHelp').text("กรุณาระบุข้อมูล");
			$("#sname").addClass("is-invalid");
			if(snamelog == 0){snamelog++;}
		}
	});

	$("#email").change(function(){ 
		if($("#email").val() != null && $("#email").val() != ""){
			if(Email_format($("#email").val())){
				$('#emailHelp').text("");

				$("#email").removeClass("is-invalid");
				if(emaillog > 0){--emaillog;}
			}else{
				$("#email").addClass("is-invalid");
				if(emaillog == 0){emaillog++;}
				$('#emailHelp').text("กรุณาระบุข้อมูลอีเมล์ให้ถูกต้อง");
			}
		}else{
			$('#emailHelp').text("");
			$("#email").removeClass("is-invalid");
			if(emaillog > 0){--emaillog;}
		}
    });
    
    //var telid = "";
	//$('#tel').keyup(function(e) {
		/*var res = this.value;
		var lengths = res.length;
		var arrtel=[];
		var arrtel0=[];
		var restel = "";
		
		if(lengths <= 10){
			if(e.key != "Backspace"){
				
				for (let index = 0; index < res.length; index++) {
					if(res[index] != "-"){
						arrtel.push(res[index]);
					}
				}
				for (let i = 0; i < arrtel.length; i++) {
					
					arrtel0.push(arrtel[i]);
					switch(i){
						case 2:
						case 5:
							arrtel0.push("-");
							break;
					}
				}
				for (let i = 0; i < arrtel0.length; i++) {
					restel += arrtel0[i];
				}
				telid = res;
			}else{

				for (let index = 0; index < res.length; index++) {
					arrtel.push(res[index]);
				}

				for (let i = 0; i < arrtel.length; i++) {
					arrtel0.push(arrtel[i]);
				}
				
				for (let i = 0; i < arrtel0.length; i++) {
					restel += arrtel0[i];
				}
			}
			this.value = restel;

		}else{ this.value = telid;	}*/
		/*if(e.key != "Backspace"){
			
			if ($.isNumeric(e.key)) {
				
				var length = telid.length;
				if(length < 10){
					telid += e.key;
					switch(length){
						case 2:
							telid += "-";
							break;
						case 6:
							telid += "-";
							break;
					}
				}else{
					this.value = telid;
				}
				this.value = telid;
			}
		}else{
			if(this.value.length == 0){
				this.value = "";
			}
			telid = this.value;
		}*/
	//});


    $("#tel").change(function(){ 
		var tel = $("#tel").val();
		
		if(tel != null && tel != ""){
            if(tel.length == 10){
                //if(Tel_format(tel)){
					console.log("ddffdgdgd");
                	//$("#tel").addClass("focusedInput");
					$('#telHelp').text("");
					$("#tel").removeClass("is-invalid");
					if(tellog > 0){--tellog;}
               // }
            }
            else{
                $("#tel").addClass("is-invalid");
                if(tellog == 0){tellog++;}
                $('#telHelp').text("กรุณาระบุข้อมูลเบอร์โทรศัพท์มือถือให้ถูกต้อง");
            }
		}else{
            $('#telHelp').text("กรุณาระบุข้อมูล");

			$("#tel").addClass("is-invalid");
			if(tellog == 0){tellog++;}
        }
    });

	$("#address").change(function(){ 
		if($("#address").val() != null && $("#address").val() != ""){
			$('#addressHelp').text("");
			$("#address").removeClass("is-invalid");
			if(addresslog > 0){--addresslog;}
		}else{
			$('#addressHelp').text("กรุณาระบุข้อมูล");
			$("#address").addClass("is-invalid");
			if(addresslog == 0){addresslog++;}
		}
	});

	$("#province").change(function(){  
		var province_id = $("#province").val();
		$("#amphue").html(null);
		var select = "<option value='' selected='false' disabled>อำเภอ / เขต</option>";
		if( province_id != null  && province_id != "" ){
			$('#provinceHelp').text("");
			$("#province").removeClass("is-invalid");
			if(provincelog > 0){--provincelog;}

			$.ajax({
				url: "<?php echo site_url('form/ajaxProvinces');?>", //ทำงานกับไฟล์นี้
				data: "province_cod=" + province_id,  //ส่งตัวแปร
				type: "POST",
				dataType: 'json',
				async:false,
				success: function(data, status) {
					$.each(data,function(index,json){
						select +="<option value="+json.id+">"+json.amphur_name+"</option>";
					});
				$("#amphue").append(select);
				},
				error: function(xhr, status, exception) { 
					//console.log(status);
				}
			});
		}else{
			$('#provinceHelp').text("กรุณาระบุข้อมูล");
			$("#province").addClass("is-invalid");
			$("#amphue").append(select);
			if(provincelog == 0){provincelog++;}
		}
	});

	$("#amphue").change(function(){ 
		$("#district").html(null);
		
		var amphue_id = $("#amphue").val();
		var select = "<option value='' selected='false' disabled >ตำบล / แขวง</option>";
		//console.log(amphue_id);
		if( amphue_id != null && amphue_id != "" ){
			$('#amphueHelp').text("");
			$("#amphue").removeClass("is-invalid");
			if(amphuelog > 0){--amphuelog;}

			$.ajax({
				url: "<?php echo site_url('form/ajaxAmphue');?>", //ทำงานกับไฟล์นี้
				data: "amphue_cod=" + amphue_id,  //ส่งตัวแปร
				type: "POST",
				dataType: 'json',
				async:false,
				success: function(data, status) {
					$.each(data,function(index,json){
						select +="<option value="+json.id+">"+json.district_name+"</option>";
					});
					$("#district").append(select);
				},
				
				error: function(xhr, status, exception) { 
					//console.log(status);
				}
			});
		}else{
			$('#amphueHelp').text("กรุณาระบุข้อมูล");
			$("#district").append(select);
			
			$("#amphue").addClass("is-invalid");
			if(amphuelog == 0){amphuelog++;}
		}
	});

	$("#district").change(function(){  
		var district_id = $("#district").val();
		if( district_id != null && district_id != "" ){
			
			$('#districtHelp').text("");
			$("#district").removeClass("is-invalid");
			if(districtlog > 0){--districtlog;}
			if(zipcodelog > 0){--zipcodelog;}
			
			$.ajax({
				url: "<?php echo site_url('form/ajaxDistricts');?>", //ทำงานกับไฟล์นี้
				data: {
					"province" : $("#province").val(),
					"amphue" : $("#amphue").val(),
					"district":  district_id
				},  //ส่งตัวแปร
				type: "POST",
				dataType: 'json',
				async:false,
				success: function(data, status) {
					var select;
					$.each(data,function(index,json){
						select = json.zipcode
					});
					$("#zipcode").removeClass("is-invalid");
					$("#zipcode").val(select);
					$('#zipcodeHelp').text("");
				},
				error: function(xhr, status, exception) { 
					//console.log(xhr);
				}
			});
		}else{
			$('#districtHelp').text("กรุณาระบุข้อมูล");
			$('#zipcodeHelp').text("กรุณาระบุข้อมูล");
			$("#district").addClass("is-invalid");
			//$("fname").addClass("form-require");
			$("#zipcode").val(null);
			if(zipcodelog == 0){zipcodelog++;}
			if(districtlog == 0){districtlog++;}
		}
	});

	//var zipcodeid = "";
	/*$('#zipcode').keyup(function(e) {
		
		if(e.key != "Backspace"){
			if ($.isNumeric(e.key)) {
				
				var length = zipcodeid.length;
				if(length < 5){
					zipcodeid += e.key;
					this.value += zipcodeid;
				}else{
					this.value = zipcodeid;
				}
				
				this.value = zipcodeid;
			}
		}else{
			if(this.value.length == 0){
				this.value = "";
			}
			zipcodeid = this.value;
		}
	});*/
	$("#zipcode").change(function(){ 
		if($("#zipcode").val() != null && $("#zipcode").val() != ""){
			if ($("#zipcode").val().length != 5) {
				$("#zipcode").addClass("is-invalid");
				$('#zipcodeHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				if(zipcodelog == 0){zipcodelog++;}
				//fieldsume();
			}else{
				$('#zipcodeHelp').text("");
				$("#zipcode").removeClass("is-invalid");
				if(zipcodelog > 0){--zipcodelog;}
			}
			
		}else{
			$('#zipcodeHelp').text("กรุณาระบุข้อมูล");
			if(zipcodelog == 0){zipcodelog++;}
			$("#zipcode").addClass("is-invalid");
		}
	});

    //var dartid = "";
	//$('#idcart').keyup(function(e) {
		/*var res = this.value;
		var lengths = res.length;
		var arridcart=[];
		var arridcart0=[];
		var residcart = "";
		if(lengths <= 17){
			if(e.key != "Backspace"){
				
				for (let index = 0; index < res.length; index++) {
					if(res[index] != "-"){
						arridcart.push(res[index]);
					}
				}
				for (let i = 0; i < arridcart.length; i++) {
					arridcart0.push(arridcart[i]);
					switch(i){
						case 0:
						case 4:
						case 9:
						case 11:
							arridcart0.push("-");
							break;
					}
				}
				for (let i = 0; i < arridcart0.length; i++) {
					residcart += arridcart0[i];
				}
				dartid = res;
			}else{
				for (let index = 0; index < res.length; index++) {
					arridcart.push(res[index]);
				}

				for (let i = 0; i < arridcart.length; i++) {
					arridcart0.push(arridcart[i]);
				}
				
				for (let i = 0; i < arridcart0.length; i++) {
					residcart += arridcart0[i];
				}
			}
			this.value = residcart;
		}else{ this.value = dartid;	}
		*/
		/*if(e.key != "Backspace"){
			if ($.isNumeric(e.key)) {
				//dartid += e.key;
				var length = dartid.length;
				if(length < 13){
					dartid += e.key;
					switch(dartid.length){
						case 1:
							dartid += "-";
							break;
						case 6:
							dartid += "-";
							break;
						case 12:
							dartid += "-";
							break;
						case 15:
							dartid += "-";
							break;
					}
				}else{
					this.value = dartid;
				}
				
				this.value = dartid;
			}
		}else{
			if(this.value.length == 0){
				this.value = "";
			}
			dartid = this.value;
		}*/
	//});
	$("#idcart").change(function(){ 
        var idcard = $("#idcart").val();
        //console.log(idcard);
		if( idcard != null && idcard != "" ){
			//console.log(idcard);
			
			$('#idcartHelp').text("");
			$("#idcart").removeClass("is-invalid");
			if(idcard == ""){ 
				$('#idcartHelp').text("กรุณาระบุข้อมูล");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				//fieldsume();
				return false;
			}
			
			if(idcard.length != 13){ 
				$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				//fieldsume();
				return false;
			}

			//var num = str_split(idcard); // function เพิ่มเติม
            var num = idcard.split("-");
            //console.log(num);
            
			var id ="";
			for (i = 0; i < num.length; i++) {
				id += num[i];
			}

			var arr = [];
			for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}

			var sum = 0;
			var total = 0;
			var digi = 13;

			for(i=0;i<12;i++){
				sum = sum + (arr[i] * digi);
				digi--;
			}

			total = ((11 - (sum % 11)) % 10);
			
			if(total == arr[12]){    
				
				
				//call api 
				/*var CORS = 'https://cors-anywhere.herokuapp.com/';
				$.ajax({
					url: CORS + 'https://passwordwolf.com/api/',
					dataType: "json",
					type: "GET",
					data: {
						length: 10,
						upper: "off",
						lower: "off",
						special: "off",
						exclude: "012345",
						repeat: 5
					},
					success: function(jsonObject, status) {
						console.log("ajax result: " + JSON.stringify(jsonObject));
					}
				});*/
				//console.log(id);
				$.ajax({
					url: "<?php echo site_url('form/ajaxIdcart');?>", //ทำงานกับไฟล์นี้
					data:  {
						'idcart':id
					},
					type: "POST",
					dataType: 'json',
					cache: false,
					//async:false,
					success: function(data, status) {
						//console.log(data);
						if(data.length > 0){
							$('#idcartHelp').text("เลขบัตรประชาชนมีการลงทะเบียนเเล้ว");
							$("#idcart").addClass("is-invalid");
							if(idcartlog == 0){idcartlog++;}
							//if(technidlog == 0){technidlog++;}
							//fieldsume();
						}else{
							
							$("#idcart").addClass("focusedInput");
							$('#idcartHelp').text("");
							$("#idcart").removeClass("is-invalid");
							//$("#idcart").addClass("focusedInput");
							
						}
					},
					error: function(xhr, status, exception) { 
						//console.log(status);
					}
				});
				$("#idcart").addClass("focusedInput");
				$("#idcart").removeClass("is-invalid");
				if(idcartlog > 0){--idcartlog;}
				return true;

			}else{   
				
				$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
				//fieldsume();
				return false;
			}
		}else{
			$('#idcartHelp').text("กรุณาระบุข้อมูล");
			$("#idcart").addClass("is-invalid");
			if(idcartlog == 0){idcartlog++;}
		}


		/*var val = $("#idcart").val();
		if( val != null && val != "" ){
			$('#idcartHelp').text("");

			var res = val.split("-");
			var id ="";
			for (i = 0; i < res.length; i++) {
				id += res[i];
			}

			if(id.length < 13){
				$('#idcartHelp').text("หมายเลขบัตรปรชาชนของคูณไม่กูกต้อง");
				if(idcartlog == 0){idcartlog++;}
				fieldsume();
			}else{

				var arr = [];
				for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}
				var strarr=[];
				for (i = 0; i < arr.length; i++) {
					var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
					if(!numberRegex.test(arr[i])) {
						strarr.push(arr[i]);
					} 
				}

				if(strarr.length > 0){
					$("#idcart").prop('required',true);
					$('#idcartHelp').text("หมายเลขบัตรปรชาชนของคูณไม่กูกต้อง");
					if(idcartlog == 0){idcartlog++;}
					fieldsume();
				}else{
					if(idcartlog > 0){--idcartlog;}
					fieldsume();
					if($('#condition').prop("checked") && fieldsume()== 0 ){
						$('#btn-submit').attr("disabled",false);
					}

					//call api 
					var CORS = 'https://cors-anywhere.herokuapp.com/';

					$.ajax({
					url: CORS + 'https://passwordwolf.com/api/',
					dataType: "json",
					type: "GET",
					data: {
						length: 10,
						upper: "off",
						lower: "off",
						special: "off",
						exclude: "012345",
						repeat: 5
					},
					success: function(jsonObject, status) {
						console.log("ajax result: " + JSON.stringify(jsonObject));
					}
					});


					//เช็คหมายเลขบัตรปรชาชน   ?????
				}
			}
		}else{
			$('#idcartHelp').text("กรุณากรอก เลขบัตรปรชาชน");
			//$("fname").addClass("form-require");
			if(idcartlog == 0){idcartlog++;}
			fieldsume();
		}*/
	});

	var technidStatus =true;
	$('#technid').keyup(function(e) {
        var dInput = this.value;
		if (dInput != null && dInput != "") {
			//$('#btn-submit').attr("disabled",true);
		}
		else{
			$('#technidHelp').text("");
			$("#technid").removeClass("is-invalid");

			$('#technidCode').text("");
			$('#technidName').text("");
			$("#collapseTechnid").collapse("hide");
			//$('#btn-submit').attr("disabled",false);
        }

        /*$('#technidHelp').text("");
			$("#technid").removeClass("is-invalid");

			$('#technidCode').text("");
			$('#technidName').text("");
			$("#collapseTechnid").collapse("hide");
			$('#btn-submit').attr("disabled",false);*/
	});


	$("#btn-technid").click(function(){ 
		var val = $("#technid").val();
		//console.log(val);
		if(val != null && val != ""){
			$('#technidHelp').text("");
			$('#technidCode').text("");
			$('#technidName').text("");
			
			$.ajax({
				url: "<?php echo site_url('form/ajaxTechn');?>", //ทำงานกับไฟล์นี้
				data:  {
					'techn_cod':val
				},
				type: "POST",
				dataType: 'json',
				cache: false,
				//async:false,
				success: function(data, status) {
					if(data == false){
						technidStatus = false;
						$("#technid").addClass("is-invalid");
						$('#technidHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
						if(technidlog == 0){technidlog++;}
						//console.log(technidlog);
						//fieldsume();
					}else{
						technidStatus = true;
						$('#technidHelp').text("");
						$("#technid").removeClass("is-invalid");
						$("#collapseTechnid").collapse("show");
						$('#technidCode').text("รหัสช่าง : "+data.AgentCode);
						$('#technidName').text("ชื่อ : "+data.AgentName+" "+data.AgentSurName);

						if(technidlog > 0){--technidlog;}
						
						$('#btn-submit').attr("disabled",false);
					}
				},
				error: function(xhr, status, exception) { 
					//console.log(status);
				}
			});
		}else{
			$('#technidHelp').text("");
			$('#technidCode').text("");
			$('#technidName').text("");
			$("#technid").removeClass("is-invalid");
			if(technidlog > 0){--technidlog;}
		}
	});


	$("#btn-submit").click(function(){ 
		//001a 
		var prefix_select = $("#prefix-select").val();
		var prefix = $("#prefix").val();
		var fname = $("#fname").val();
		var sname = $("#sname").val();
		var email = $("#email").val();

		var telSplit = $("#tel").val().split("-");
        var tel ="";
		for (i = 0; i < telSplit.length; i++) {
			tel += telSplit[i];
		}
		//var tel = $("#tel").val();
		var address = $("#address").val();
		var province = $("#province").val();
		var amphue = $("#amphue").val();
		var district = $("#district").val();
		var zipcode = $("#zipcode").val();
		
		var idcartSplit = $("#idcart").val().split("-");
        var idcart ="";
		for (i = 0; i < idcartSplit.length; i++) {
			idcart += idcartSplit[i];
		}

		//var idcart = $("#idcart").val();
		var technid = $("#technid").val();
		//var condition = $("#condition").val();

		
		if(prefix_select == null || prefix_select == ""){
			$('#prefix-selectHelp').text("กรุณาระบุข้อมูล");
			$("#prefix-select").addClass("is-invalid");
		}
		
		if(prefix == null || prefix == ""){
			$('#prefixHelp').text("กรุณาระบุข้อมูล");
			$("#prefix").addClass("is-invalid");
			if(prefixlog == 0){prefixlog++;}
			//fieldsume();
		}

		if(fname == null || fname == ""){
			$('#fnameHelp').text("กรุณาระบุข้อมูล");
			$("#fname").addClass("is-invalid");
			if(fnamelog == 0){fnamelog++;}
			//fieldsume();
		}

		if(sname == null || sname == ""){
			$('#snameHelp').text("กรุณาระบุข้อมูล");
			$("#sname").addClass("is-invalid");
			if(snamelog == 0){snamelog++;}
			//fieldsume();
		}

		if(tel == null || tel == ""){
			$('#telHelp').text("กรุณาระบุข้อมูล");
			$("#tel").addClass("is-invalid");
			if(tellog == 0){tellog++;}
			//fieldsume();
		}else{
			if(tel.length != 10){
				$('#telHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#tel").addClass("is-invalid");
				if(tellog == 0){tellog++;}
			}else{
				$('#telHelp').text("");
				$("#tel").removeClass("is-invalid");
				if(tellog > 0){--tellog;}
			}
		}

		if(address == null || address == ""){
			$('#addressHelp').text("กรุณาระบุข้อมูล");
			$("#address").addClass("is-invalid");
			if(addresslog == 0){addresslog++;}
			//fieldsume();
		}

		if(province == null || province == ""){
			$('#provinceHelp').text("กรุณาระบุข้อมูล");
			$("#province").addClass("is-invalid");
			if(provincelog == 0){provincelog++;}
			//fieldsume();
		}
		if(amphue == null || amphue == ""){
			$('#amphueHelp').text("กรุณาระบุข้อมูล");
			$("#amphue").addClass("is-invalid");
			if(amphuelog == 0){amphuelog++;}
			//fieldsume();
		}
		if(district == null || district == ""){
			$("#district").addClass("is-invalid");
			$('#districtHelp').text("กรุณาระบุข้อมูล");
			if(districtlog == 0){districtlog++;}
			//fieldsume();
		}
		if(zipcode == null || zipcode == ""){
			$("#zipcode").addClass("is-invalid");
			$('#zipcodeHelp').text("กรุณาระบุข้อมูล");
			if(zipcodelog == 0){zipcodelog++;}
			//fieldsume();
		}else{
			if (zipcode.length != 5) {
				$("#zipcode").addClass("is-invalid");
				$('#zipcodeHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				if(zipcodelog == 0){zipcodelog++;}
				//fieldsume();
			}else{
				$('#zipcodeHelp').text("");
				$("#zipcode").removeClass("is-invalid");
				if(zipcodelog > 0){--zipcodelog;}
			}
		}

		if(idcart == null || idcart == ""){
			$("#idcart").addClass("is-invalid");
			$('#idcartHelp').text("กรุณาระบุข้อมูล");
			if(idcartlog == 0){idcartlog++;}
			//fieldsume();
		}else{
			if(idcart.length != 13){
				$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
				$("#idcart").addClass("is-invalid");
				if(idcartlog == 0){idcartlog++;}
			}else{
				//var num = idcard.split("-");
				//console.log(num);
				
				var id =idcart;
				

				var arr = [];
				for (i = 0; i < id.length; i++) {arr.push(id.charAt(i));}

				var sum = 0;
				var total = 0;
				var digi = 13;

				for(i=0;i<12;i++){
					sum = sum + (arr[i] * digi);
					digi--;
				}

				total = ((11 - (sum % 11)) % 10);
				
				if(total == arr[12]){  
					$.ajax({
						url: "<?php echo site_url('form/ajaxIdcart');?>", //ทำงานกับไฟล์นี้
						data:  {
							'idcart':idcart
						},
						type: "POST",
						dataType: 'json',
						cache: false,
						//async:false,
						success: function(data, status) {
							//console.log(data);
							if(data.length > 0){
								$('#idcartHelp').text("เลขบัตรประชาชนมีการลงทะเบียนเเล้ว");
								$("#idcart").addClass("is-invalid");
								if(idcartlog == 0){idcartlog++;}
								//if(technidlog == 0){technidlog++;}
								//fieldsume();
							}else{
								$('#idcartHelp').text("");
								$("#idcart").removeClass("is-invalid");
								if(idcartlog > 0){--idcartlog;}
							}
						},
						error: function(xhr, status, exception) { 
							//console.log(status);
						}
					});
					//return true;

				}else{   
					
					$('#idcartHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
					$("#idcart").addClass("is-invalid");
					if(idcartlog == 0){idcartlog++;}
					//fieldsume();
					//return false;
				}
			}
		}

		if(technid == null || technid == ""){
			$("#technid").removeClass("is-invalid");
			$('#technidHelp').text("");
			if(technidlog > 0){technidlog;}
			//fieldsume();
		}else{
			//var val = $("#technid").val();
			if(technid != null && technid != ""){
				$('#technidHelp').text("");
				
				$.ajax({
					url: "<?php echo site_url('form/ajaxTechn');?>", //ทำงานกับไฟล์นี้
					data:  {
						'techn_cod':technid
					},
					type: "POST",
					dataType: 'json',
					cache: false,
					//async:false,
					success: function(data, status) {
						if(data == false){
							$("#technid").addClass("is-invalid");
							$('#technidHelp').removeClass("text-muted-y");
							$('#technidHelp').addClass("text-muted");
							$("#collapseTechnid").collapse("hide");
							$('#technidHelp').text("กรุณาระบุข้อมูลให้ถูกต้อง");
							if(technidlog == 0){technidlog++;}
							//fieldsume();
						}else{
							$('#technidHelp').text("");
							$("#technid").removeClass("is-invalid");
							$("#collapseTechnid").collapse("show");
							$('#technidCode').text("รหัสช่าง : "+data.AgentCode);
							$('#technidName').text("ชื่อ : "+data.AgentName+" "+data.AgentSurName);

							if(technidlog > 0){--technidlog;}
							
						}
					},
					error: function(xhr, status, exception) { 
						//console.log(status);
					}
				});
			}else{
				$("#technid").removeClass("is-invalid");
				$('#technidHelp').text("");
				
				if(technidlog > 0){--technidlog;}
				
			}
		}
		//console.log(tel);

		//console.log(fieldsume());
		if(fieldsume() == 0 ){
			$.ajax({
				url: "<?php echo site_url('form/insert');?>", //ทำงานกับไฟล์นี้
				data: {
					"prefix" : prefix,
					"fname" : fname,
					"sname" : sname,
					"email" : email,
					"tel" : tel,
					"address" : address,
					"province" : province,
					"amphue" : amphue,
					"district" : district,
					"zipcode" : zipcode,
					"idcart" : idcart,
					"technid" : technid
					//"condition":  condition
				},  //ส่งตัวแปร
				type: "POST",
				dataType: 'json',
				async:false,
				success: function(data, status) {
					//console.log(data);
					if(data){
						$('#SaveModal').modal('show');
						/*$("#prefix-select").val(0); 
						//$("#prefix-select").val(null);
						$("#prefix").val(null);
						$("#fname").val(null);
						$("#sname").val(null);
						$("#email").val(null);
						$("#tel").val(null);
						$("#address").val(null);
						$("#province").val(null);
						$("#amphue").val(null);
						$("#district").val(null);
						$("#zipcode").val(null);
						$("#idcart").val(null);
						$("#technid").val(null);
						//$('#condition').prop("checked",false);
						//$('#btn-submit').attr("disabled",true);

						$('#idcartHelp').text("");
						$("#idcart").removeClass("is-invalid");*/
					}
				},
				
				error: function(xhr, status, exception) { 
					//console.log(status);
				}
			});
		}else{
			$("#SaveFailModal").modal("show");
		}

	});


	$("#btn-cancel").click(function(){
		$('#cancelModal').modal('show'); 
	});

	$(".btn-cancel").click(function(){ 
		/*$("#prefix-select").val(0);
		$("#prefix").val(null);
		$('#prefix-selectHelp').text("");
		$("#prefix-select").removeClass("is-invalid");
		
		$("#fname").val(null);
		$('#fnameHelp').text("");
		$("#fname").removeClass("is-invalid");

		$("#sname").val(null);
		$('#snameHelp').text("");
		$("#sname").removeClass("is-invalid");

		$("#email").val(null);
		$("#tel").val(null);
		$('#telHelp').text("");
		$("#tel").removeClass("is-invalid");

		$("#address").val(null);
		$('#addressHelp').text("");
		$("#address").removeClass("is-invalid");

		$("#province").val(null);
		$('#provinceHelp').text("");
		$("#province").removeClass("is-invalid");

		$("#amphue").val(null);
		$('#amphueHelp').text("");
		$("#amphue").removeClass("is-invalid");

		$("#district").val(null);
		$('#districtHelp').text("");
		$("#district").removeClass("is-invalid");

		$("#zipcode").val(null);
		$('#zipcodeHelp').text("");
		$("#zipcode").removeClass("is-invalid");

		$("#idcart").val(null);
		$('#idcartHelp').text("");
		$("#idcart").removeClass("is-invalid");
		
		$("#technid").val(null);
		//$('#condition').prop("checked",false);

        $('#technidHelp').text("");
        $("#btn-technid").removeClass("is-invalid");
        $("#technid").removeClass("is-invalid");

		$('#technidCode').text("");
		$('#technidName').text("");
		$("#collapseTechnid").collapse("hide");
		$('#btn-submit').attr("disabled",false);*/
		
		//window.location.href = "<?php// echo site_url().'form/condition';?>";
		window.location.href = "<?php echo site_url().'form';?>";
		
	});

	
	$("#SaveModal").click(function(){ 
		$('#technidCode').text("");
		$('#technidName').text("");
		$("#collapseTechnid").collapse("hide");
	});

	$("#SaveModal-btn").click(function(){ 
		//window.location.href = "<?php //echo site_url().'form/condition';?>";
		window.location.href = "<?php echo site_url().'form';?>";
	});

	$("#btn-fails").click(function(){ 
		if (technidStatus == false) {
			$("#technid").val(null);
		}
	});

	$(".number-type").keypress(function (e) {
		if (/\d+|[/b]+/i.test(e.key) ){
		//if (/[0-9]/i.test(e.key) ){
			//console.log("character accepted: " + e.key)
		} else {
			//console.log("illegal character detected: "+ e.key)
			return false;
		}
	});

	function fieldsume(){ 
		intlog =  prefixlog + fnamelog + snamelog + addresslog + emaillog + tellog+ provincelog +amphuelog +zipcodelog +districtlog +idcartlog +technidlog+conditionlog;
		return intlog;
	}

    function Tel_format(tels){
		var res =  false;
		var testTel = /^[0-9]+-([0-9])+-[0-9]{2,4}$/i;
		//var testTel = /^[0-9]{2,4}$/i;
		if (testTel.test(tels)){
			res = true;
		}else{
			res = false;
		}
		return res;
    }

	function Email_format(email){
		var res =  false;
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (testEmail.test(email)){
			res = true;
		}else{
			res = false;
		}
		return res;
    }
    

	</script>

<script langquage='javascript'>
url=document.location;
console.log(url);
/*if ((url=="http://www.firstdomainname.com") || (url=="http://www.firstdomainname.com/"))
{
document.write("<head><title>www.firstdomainname.com</title></head>");
document.write('<frameset cols="*">');
document.write('<frame src="http://www.yourdomainname.com/firstdomainname">');
document.write("</frameset>");
}
else if (url=="http://www.seconddomainname.com") || (url=="http://www.seconddomainname.com/"))
{
document.write("<head><title>www.seconddomainname.com</title></head>");
document.write('<frameset cols="*">');
document.write('<frame src="http://www.yourdomainname.com/seconddomainname">');
document.write("</frameset>");
}
else
{
document.write('<frameset cols="*">');
document.write('<frame src="http://www.yourdomainname.com/underconstruction.html">');
document.write("</frameset>");
}*/
</script>

	<!--<script src="<?php //echo base_url('./assete/js/js.js');?>"></script> -->